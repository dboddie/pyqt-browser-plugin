#ifndef PYQTNPBINDABLE_H
#define PYQTNPBINDABLE_H

#include <QWidget>
#include "qtbrowserplugin.h"

class PyQtNPBindable : public QWidget, public QtNPBindable
{
    Q_OBJECT

public:
    PyQtNPBindable(QWidget *parent = 0) : QWidget(parent) {}

private:
    PyQtNPBindable(const PyQtNPBindable &);
};

#endif
