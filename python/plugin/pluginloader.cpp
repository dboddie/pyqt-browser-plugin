/*
 * This is the QtBrowserPlugin Solution plugin that collects all the Python
 * plugins it can find and exposes them to browsers that use the Netscape
 * plugin API.
 *
 * Copyright (c) 2008 Riverbank Computing Limited <info@riverbankcomputing.com>
 * Copyright (c) 2009 David Boddie
 * 
 * This file is part of PyQt.
 * 
 * This file may be used under the terms of the GNU General Public
 * License versions 2.0 or 3.0 as published by the Free Software
 * Foundation and appearing in the files LICENSE.GPL2 and LICENSE.GPL3
 * included in the packaging of this file.  Alternatively you may (at
 * your option) use any later version of the GNU General Public
 * License if such license has been publicly approved by Riverbank
 * Computing Limited (or its successors, if any) and the KDE Free Qt
 * Foundation. In addition, as a special exception, Riverbank gives you
 * certain additional rights. These rights are described in the Riverbank
 * GPL Exception version 1.1, which can be found in the file
 * GPL_EXCEPTION.txt in this package.
 * 
 * Please review the following information to ensure GNU General
 * Public Licensing requirements will be met:
 * http://trolltech.com/products/qt/licenses/licensing/opensource/. If
 * you are unsure which license is appropriate for your use, please
 * review the following information:
 * http://trolltech.com/products/qt/licenses/licensing/licensingoverview
 * or contact the sales department at sales@riverbankcomputing.com.
 * 
 * This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
 * INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE. Trolltech reserves all rights not expressly
 * granted herein.
 * 
 * This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
 * WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */


#include "qtbrowserplugin.h"
#include <Python.h>

#include <QDebug>
#include <QtGlobal>
#include <QtPlugin>
#include <QCoreApplication>
#include <QDir>
#include <QLibrary>
#include <QStringList>


#define PY_MODULE_CODE \
   "import imp, os, platform, sys, types\n" \
   "dirs = []\n" \
   "if platform.system() == 'Linux':\n" \
   "    if os.getenv('MOZILLA_HOME'):\n" \
   "        dirs.append(os.path.join(os.getenv('MOZILLA_HOME'), 'plugins'))\n" \
   "    dirs.append(os.path.join(os.path.expanduser('~'), '.mozilla', 'plugins'))\n" \
   "    dirs.append(os.path.join(os.path.expanduser('~'), '.netscape', 'plugins'))\n" \
   "elif platform.system() == 'Windows':\n" \
   "    dirs.append(os.path.join(os.path.expanduser('~'), 'Application Data', 'Mozilla', 'Plugins'))\n" \
   "    dirs.append('c:\\program files\\mozilla firefox\\plugins')\n" \
   "    dirs.append('c:\\program files\\opera\\program\\plugins')\n" \
   "    dirs.append('c:\\program files\\netscape\\netscape browser\\plugins')\n" \
   "elif platform.system() == 'Darwin':\n" \
   "    dirs.append(os.path.join(os.path.expanduser('~'), 'Library', 'Internet Plug-Ins'))\n" \
   "    dirs.append('/Library/Internet Plug-Ins')\n" \
   "\n" \
   "sys.path += dirs\n" \
   "\n" \
   "from QtBrowserPlugin import QtNPFactory\n" \
   "\n" \
   "class QtNPClassList(QtNPFactory):\n" \
   "\n" \
   "    def __init__(self):\n" \
   "        QtNPFactory.__init__(self)\n" \
   "        self.factories = {}\n" \
   "        for dir in dirs:\n" \
   "            for name in os.listdir(dir):\n" \
   "                stem, suffix = os.path.splitext(name)\n" \
   "                if suffix != '.py':\n" \
   "                    continue\n" \
   "                try:\n" \
   "                    f, path, details = imp.find_module(stem, [dir])\n" \
   "                    module = imp.load_module(stem, f, path, details)\n" \
   "                except ImportError:\n" \
   "                    print 'Failed to import', os.path.join(dir, stem)\n" \
   "                    continue\n" \
   "                for obj in module.__dict__.values():\n" \
   "                    if hasattr(obj, '__bases__') and obj != QtNPFactory and issubclass(obj, QtNPFactory):\n" \
   "                        factory = obj()\n" \
   "                        for mimeType in factory.mimeTypes():\n" \
   "                            mimeType = str(mimeType).split(':')[0]\n" \
   "                            self.factories[mimeType] = factory\n" \
   "\n" \
   "    def mimeTypes(self):\n" \
   "        types = []\n" \
   "        for factory in self.factories.values():\n" \
   "            types += factory.mimeTypes()\n" \
   "        return types\n" \
   "\n" \
   "    def createObject(self, type):\n" \
   "        factory = self.factories.get(str(type))\n" \
   "        if factory:\n" \
   "            return factory.createObject(type)\n" \
   "        else:\n" \
   "            return None\n" \
   "\n" \
   "    def pluginName(self):\n" \
   "        return 'PyQt4 Plugin'\n" \
   "\n" \
   "    def pluginDescription(self):\n" \
   "        return 'A demonstration plugin that embeds PyQt4 widgets in Web browsers.'\n" \
   "\n"

// Return the named attribute object from the named module.
PyObject *getModuleAttr(const char *module, const char *attr)
{
#if PY_VERSION_HEX >= 0x02050000
    PyObject *mod = PyImport_ImportModule(module);
#else
    PyObject *mod = PyImport_ImportModule(const_cast<char *>(module));
#endif

    if (!mod)
    {
        PyErr_Print();
        return 0;
    }

#if PY_VERSION_HEX >= 0x02050000
    PyObject *obj = PyObject_GetAttrString(mod, attr);
#else
    PyObject *obj = PyObject_GetAttrString(mod, const_cast<char *>(attr));
#endif

    Py_DECREF(mod);

    if (!obj)
    {
        PyErr_Print();
        return 0;
    }

    return obj;
}

// Construct the collection of Python widgets.
QtNPFactory *qtns_instantiate()
{
    // The sip.unwrapinstance object if we need it.
    PyObject *sip_unwrapinstance = 0;

    // Make sure the interpreter is loaded and initialised.  Leave this as
    // late as possible.
    if (!Py_IsInitialized())
    {
        QLibrary library(PYTHON_LIB);

        library.setLoadHints(QLibrary::ExportExternalSymbolsHint);

        if (!library.load())
            return 0;

        Py_Initialize();
    }

    
    // Make sure we have sip.unwrapinstance.
    if (!sip_unwrapinstance)
    {
        sip_unwrapinstance = getModuleAttr("sip", "unwrapinstance");

        if (!sip_unwrapinstance)
            return 0;
    }

    PyObject *pyModule = PyImport_AddModule("__main__");
    PyObject *pyDict = PyModule_GetDict(pyModule);
    // pyDict is a borrowed reference.

    if (!PyRun_String(PY_MODULE_CODE, Py_file_input, pyDict, pyDict))
    {
        PyErr_Print();
        return 0;
    }

    PyObject *pyFactory = PyDict_GetItemString(pyDict, "QtNPClassList");
    // pyFactory is a borrowed reference.

    // Create the plugin instance.  Note that we don't give it a
    // parent, which make things easier.  It also means that Python
    // owns the instance so we don't decrement the reference count
    // so that it doesn't get garbage collected.
    PyObject *plugobj = PyObject_CallObject(pyFactory, NULL);

    if (!plugobj)
    {
        PyErr_Print();
        return 0;
    }

    // Get the address of the C++ instance.
    PyObject *plugaddr = PyObject_CallFunctionObjArgs(sip_unwrapinstance, plugobj, NULL);

    if (!plugaddr)
    {
        Py_DECREF(plugobj);
        PyErr_Print();
        return 0;
    }

    void *addr = PyLong_AsVoidPtr(plugaddr);
    Py_DECREF(plugaddr);

    return reinterpret_cast<QtNPFactory *>(addr);
}
