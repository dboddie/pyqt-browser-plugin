from PyQt4.QtGui import *
from QtBrowserPlugin import QtNPFactory, PyQtNPBindable

class Factory(QtNPFactory):

    def mimeTypes(self):
    
        return ["text/plain:txt:Plain text"]
    
    def createObject(self, type):
    
        if type == "text/plain":
            return TextEdit()
    
    def pluginName(self):
        return ""
    
    def pluginDescription(self):
        return ""


class TextEdit(PyQtNPBindable):

    def __init__(self, parent = None):
    
        PyQtNPBindable.__init__(self, parent)
        
        self.editor = QTextEdit()
        layout = QVBoxLayout(self)
        self.addWidget(self.editor)
    
    def readData(self, source, format):
    
        if not source.open(source.ReadOnly | source.Text):
            return False
        
        self.editor.setPlainText(source.readAll())
        return True
