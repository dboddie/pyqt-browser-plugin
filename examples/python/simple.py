from PyQt4.QtGui import *
from QtBrowserPlugin import QtNPFactory

class Factory(QtNPFactory):

    def mimeTypes(self):
    
        return ["text/special:txt:Plain text"]
    
    def createObject(self, type):
    
        if type == "text/special":
            return QLabel("Hello world")
    
    def pluginName(self):
        return ""
    
    def pluginDescription(self):
        return ""
