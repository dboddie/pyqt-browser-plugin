import sys
from PyQt4.QtCore import QT_VERSION_STR
from PyQt4.pyqtconfig import Configuration
from PyQt4.QtGui import *
from QtBrowserPlugin import QtNPFactory

class Factory(QtNPFactory):

    def mimeTypes(self):
    
        return ["text/information:info:Information about the Python configuration"]
    
    def createObject(self, type):
    
        if type == "text/information":
            config = Configuration()
            browser = QTextBrowser()
            browser.setPlainText(
                "Python: "+str(sys.version)+"\n"
                "Qt: "+QT_VERSION_STR+"\n"
                "PyQt: "+config.pyqt_version_str
                )
            return browser
    
    def pluginName(self):
        return ""
    
    def pluginDescription(self):
        return ""
